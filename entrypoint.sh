#!/usr/bin/env bash

if [ ! -n "$REMOTE_PATH" ]; then
  >&2 echo "REMOTE_PATH must be present"
fi

echo $REMOTE_PATH

if [[ ! -f "/etc/nginx/nginx.conf.plain" ]]; then
  cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.plain
fi

sed '/http {/a resolver 8.8.8.8;' /etc/nginx/nginx.conf.plain > /etc/nginx/nginx.conf

cat << EOF > /etc/nginx/sites-enabled/default
server {
 listen 80;
 server_name _;
 location / {
   proxy_pass $REMOTE_PATH\$http_host\$request_uri;
   proxy_intercept_errors on;
   proxy_redirect off;
   error_page 404 =200 /_spa_redirect;
 }
 location /_spa_redirect {
   proxy_pass $REMOTE_PATH\$http_host/index.html;
 }
}
EOF

/usr/sbin/nginx
